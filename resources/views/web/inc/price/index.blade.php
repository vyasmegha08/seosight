@extends('web.layouts.app')
@section('main_section')




<div class="mCustomScrollbar" data-mcs-theme="dark">

	<div class="popup right-menu">

		<div class="right-menu-wrap">

			<div class="user-menu-close js-close-aside">
				<a href="#" class="user-menu-content  js-clode-aside">
					<span></span>
					<span></span>
				</a>
			</div>

			<div class="logo">
				<a href="index.html" class="full-block-link"></a>
				<img src="img/logo-eye.png" alt="Seosight">
				<div class="logo-text">
					<div class="logo-title">Seosight</div>
				</div>
			</div>

			<p class="text">Investigationes demonstraverunt lectores legere me lius quod
				ii legunt saepius est etiam processus dynamicus.
			</p>

		</div>

		<div class="widget login">

			<h4 class="login-title">Sign In to Your Account</h4>
			<input class="email input-standard-grey" placeholder="Username or Email" type="text">
			<input class="password input-standard-grey" placeholder="Password" type="password">
			<div class="login-btn-wrap">

				<div class="btn btn-medium btn--dark btn-hover-shadow">
					<span class="text">login now</span>
					<span class="semicircle"></span>
				</div>

				<div class="remember-wrap">

					<div class="checkbox">
						<input id="remember" type="checkbox" name="remember" value="remember">
						<label for="remember">Remember Me</label>
					</div>

				</div>

			</div>

			<div class="helped">Lost your password?</div>
			<div class="helped">Register Now</div>

		</div>


		<div class="widget contacts">

			<h4 class="contacts-title">Get In Touch</h4>
			<p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus,
				vitae donec vestibulum enim, tincidunt massa sit, convallis ipsum.
			</p>

			<div class="contacts-item">
				<img src="img/contact4.png" alt="phone">
				<div class="content">
					<a href="#" class="title">8 800 567.890.11</a>
					<p class="sub-title">Mon-Fri 9am-6pm</p>
				</div>
			</div>

			<div class="contacts-item">
				<img src="img/contact5.png" alt="phone">
				<div class="content">
					<a href="#" class="title">info@seosight.com</a>
					<p class="sub-title">online support</p>
				</div>
			</div>

			<div class="contacts-item">
				<img src="img/contact6.png" alt="phone">
				<div class="content">
					<a href="#" class="title">Melbourne, Australia</a>
					<p class="sub-title">795 South Park Avenue</p>
				</div>
			</div>

		</div>

	</div>

</div>

<!-- ... End Right-menu -->

<div class="content-wrapper">

	<div class="overlay_search">
		<div class="container">
			<div class="row">
				<div class="form_search-wrap">
					<form>
						<input class="overlay_search-input" placeholder="Type and hit Enter..." type="text">
						<a href="#" class="overlay_search-close">
							<span></span>
							<span></span>
						</a>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid" style="background-image: url('img/background-element.png')">
		<div class="header-elements" style="min-height: 350px; padding: 140px 70px; text-align: center;">
			<h1 class="title" style="color: #fff">Pricing Tables</h1>
            
		</div>
	</div>
    <!-- my slider -->
    <div class="container">
        <div class="row d-flex">
            <div class="col-lg-6">
                <h1 class="respon" >Responsic Web Degin</h1>
                <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram,
				    anteposuerit litterarum formas humanitatis placerat facer possim assum
                    Mirum est notare quam littera gothica, quam nunc putamus parum claram,
				    anteposuerit litterarum formas humanitatis placerat facer possim assum.</p>
            </div>
            <div class="col-lg-6">
                <div class="myimg">
                <img src="img/description-mac.png" alt="" width="400" height="400" class="img"  >
                </div>
            </div>
        </div>

    </div>




	<div class="container-fluid">
		<div class="row">
			<div class="pricing-tables medium-padding120 bg-border-color">
				<div class="container">
                
					<div class="row">
						<div class="pricing-tables-wrap">
							
                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

<div class="pricing-tables-item">
    <div class="pricing-tables-icon">
        <img src="{{url('img/pricing1.png')}}" alt="personal">
    </div>
    <a href="#" class="pricing-title">Basic Bussiness Plan</a>
    <h4 class="rate">$49.99</h4>
    <ul class="pricing-tables-position">
        <li class="position-item">
            Free Domain (.in)
        </li>
        <li class="position-item">
            
             Hosting (01 Year Free)
        </li>
        <li class="position-item">
            
            4 Page (Dynamic Website)
        </li>
        <li class="position-item">
            Images + Video Gallery
        </li>
        <li class="position-item">
            
            Limited Images & Videos
        </li>
        <li class="position-item">
            
            Limited* (Space)
        </li>
        <li class="position-item">
            
            SEO Ready Website
        </li>
        <li class="position-item">
            
            100% Responsive Website
        </li>
        <li class="position-item">
            
            Hit Counter + WhatsApp Button
        </li>
        <li class="position-item">
            
            Business Email ID
        </li>
        <li class="position-item">
            
            Payment Gateway Integration
        </li>
        <li class="position-item">
            
            Social Media Integration
        </li>
        <li class="position-item">
            
            SSL Certificate
        </li>
        <li class="position-item">
            
            Mobile Friendly Website
        </li>
        <li class="position-item">
            
            Enquiry / Feedback Form
        </li>
        <li class="position-item">
            
            Support (Phone/Email/Chat)
        </li>
        <li class="position-item">
            
            One Year Technical Support
        </li>
        
    </ul>
    
    <a href="20_checkout.html" class="btn btn-medium btn--dark">
        <span class="text">Order Now!</span>
        <span class="semicircle"></span>
    </a>
    <img src="img/pricing-dots.png" class="dots" alt="dots">
</div>

</div>

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="pricing-tables-item">
    <div class="pricing-tables-icon">
        <img src="img/pricing2.png" alt="personal">
    </div>
    <a href="#" class="pricing-title">SuperBusinessPlan</a>
    <h4 class="rate">$99.99</h4>
    <ul class="pricing-tables-position">
        <li class="position-item">
            Free Domain (.in)
        </li>
        <li class="position-item">
            
             Hosting (01 Year Free)
        </li>
        <li class="position-item">
            
             6 Page (Dynamic Website)
        </li>
        <li class="position-item">
            Images + Video Gallery
        </li>
        <li class="position-item">
            
            Unlimited Images & Videos
        </li>
        <li class="position-item">
            
             Unlimited* (Space)
        </li>
        <li class="position-item">
            
            SEO Ready Website
        </li>
        <li class="position-item">
            
            100% Responsive Website
        </li>
        <li class="position-item">
            
            Hit Counter + WhatsApp Button
        </li>
        <li class="position-item">
            
            Business Email ID
        </li>
        <li class="position-item">
            
            Payment Gateway Integration
        </li>
        <li class="position-item">
            
            Social Media Integration
        </li>
        <li class="position-item">
            
            SSL Certificate
        </li>
        <li class="position-item">
            
            Mobile Friendly Website
        </li>
        <li class="position-item">
            
            Enquiry / Feedback Form
        </li>
        <li class="position-item">
            
            Support (Phone/Email/Chat)
        </li>
        <li class="position-item">
            
            One Year Technical Support
        </li>

    </ul>
    
    <a href="20_checkout.html" class="btn btn-medium btn--dark">
        <span class="text">Best Business Plan</span>
        <span class="semicircle"></span>
    </a>

    <img src="img/pricing-dots.png" class="dots" alt="dots">
</div>
</div>

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="pricing-tables-item">
    <div class="pricing-tables-icon">
        <img src="img/pricing3.png" alt="personal">
    </div>
    <a href="#" class="pricing-title">Professional</a>
    <h4 class="rate">$169.99</h4>
    <ul class="pricing-tables-position">
    <li class="position-item">
            Free Domain (.in)
        </li>
        <li class="position-item">
            
             Hosting (01 Year Free)
        </li>
        <li class="position-item">
            
             L Pages* (Dynamic Website)
        </li>
        <li class="position-item">
            Images + Video Gallery
        </li>
        <li class="position-item">
            
            Unlimited Images & Videos
        </li>
        <li class="position-item">
            
            Unlimited* (Bandwidth/ Space)
        </li>
        <li class="position-item">
            
            SEO Ready Website
        </li>
        <li class="position-item">
            
            100% Responsive Website
        </li>
        <li class="position-item">
            
            Hit Counter + WhatsApp Button
        </li>
        <li class="position-item">
            
            Business Email ID
        </li>
        <li class="position-item">
            
            Payment Gateway Integration
        </li>
        <li class="position-item">
            
            Social Media Integration
        </li>
        <li class="position-item">
            
            SSL Certificate
        </li>
        <li class="position-item">
            
            Mobile Friendly Website
        </li>
        <li class="position-item">
            
            Enquiry / Feedback Form
        </li>
        <li class="position-item">
            
            Support (Phone/Email/Chat)
        </li>
        <li class="position-item">
            
            One Year Technical Support
        </li>
    </ul>
    
    <a href="20_checkout.html" class="btn btn-medium btn--dark">
        <span class="text">Order Now!</span>
        <span class="semicircle"></span>
    </a>

    <img src="img/pricing-dots.png" class="dots" alt="dots">
</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="container-fluid">
		<div class="row">
			<div class="pricing-tables pricing-tables-head medium-padding120 bg-white-color">
				<div class="container">

					<div class="row">

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">

							<div class="pricing-tables-item">

								<div class="pricing-head bg-primary-color"></div>

								<div class="pricing-content-wrap">

									<div class="pricing-tables-icon">
										<img src="img/pricing1.png" alt="personal">
									</div>
									<a href="#" class="pricing-title">Personal</a>
									<ul class="pricing-tables-position">
										<li class="position-item">
											<span class="count">5</span>
											Analytics Campaigns
										</li>
										<li class="position-item">
											<span class="count">300</span>
											Keywords
										</li>
										<li class="position-item">
											<span class="count">250,000</span>
											Crawled Pages
										</li>
										<li class="position-item">
											-
										</li>
										<li class="position-item">
											<span class="count">15</span>
											Social Accounts
										</li>
									</ul>
									<h4 class="rate">$49.99</h4>
									<a href="#" class="btn btn-medium btn--dark">
										<span class="text">Order Now!</span>
										<span class="semicircle"></span>
									</a>

								</div>

							</div>

						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
							<div class="pricing-tables-item">

								<div class="pricing-head bg-secondary-color"></div>

								<div class="pricing-content-wrap">

									<div class="pricing-tables-icon">
										<img src="img/pricing2.png" alt="personal">
									</div>
									<a href="#" class="pricing-title">Webmaster</a>
									<ul class="pricing-tables-position">
										<li class="position-item">
											<span class="count">25</span>
											Analytics Campaigns
										</li>
										<li class="position-item">
											<span class="count">1,900</span>
											Keywords
										</li>
										<li class="position-item">
											<span class="count">1,250,000</span>
											Crawled Pages
										</li>
										<li class="position-item include">
											Includes Branded Reports
										</li>
										<li class="position-item">
											<span class="count">50</span>
											Social Accounts
										</li>
									</ul>
									<h4 class="rate">$99.99</h4>
									<a href="#" class="btn btn-medium btn--dark">
										<span class="text">Order Now!</span>
										<span class="semicircle"></span>
									</a>

								</div>
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">

							<div class="pricing-tables-item">

								<div class="pricing-head bg-orange-color"></div>

								<div class="pricing-content-wrap">

									<div class="pricing-tables-icon">
										<img src="img/pricing3.png" alt="personal">
									</div>
									<a href="#" class="pricing-title">Professional</a>
									<ul class="pricing-tables-position">
										<li class="position-item">
											<span class="count">100</span>
											Analytics Campaigns
										</li>
										<li class="position-item">
											<span class="count">7500</span>
											Keywords
										</li>
										<li class="position-item">
											<span class="count">1,250,000</span>
											Crawled Pages
										</li>
										<li class="position-item include">
											Includes Branded Reports
										</li>
										<li class="position-item">
											<span class="count">150</span>
											Social Accounts
										</li>
									</ul>
									<h4 class="rate">$169.99</h4>
									<a href="#" class="btn btn-medium btn--dark">
										<span class="text">Order Now!</span>
										<span class="semicircle"></span>
									</a>

								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="pricing-tables pricing-tables-classic medium-padding120 bg-border-color">
				<div class="container">

					<div class="row">

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

							<div class="pricing-tables-item">

								<div class="pricing-tables-icon">
									<img src="img/pricing1.png" alt="personal">
								</div>
								<a href="#" class="pricing-title">Personal</a>
								<ul class="pricing-tables-position">
									<li class="position-item">
										<span class="count">5</span>
										Analytics Campaigns
									</li>
									<li class="position-item">
										<span class="count">300</span>
										Keywords
									</li>
									<li class="position-item">
										<span class="count">250,000</span>
										Crawled Pages
									</li>
									<li class="position-item">
										-
									</li>
									<li class="position-item">
										<span class="count">15</span>
										Social Accounts
									</li>
								</ul>
								<h4 class="rate">$49.99</h4>
								<a href="#" class="btn btn-medium btn--dark">
									<span class="text">Order Now!</span>
									<span class="semicircle"></span>
								</a>

							</div>

						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="pricing-tables-item">

								<div class="pricing-tables-icon">
									<img src="img/pricing2.png" alt="personal">
								</div>
								<a href="#" class="pricing-title">Webmaster</a>
								<ul class="pricing-tables-position">
									<li class="position-item">
										<span class="count">25</span>
										Analytics Campaigns
									</li>
									<li class="position-item">
										<span class="count">1,900</span>
										Keywords
									</li>
									<li class="position-item">
										<span class="count">1,250,000</span>
										Crawled Pages
									</li>
									<li class="position-item include">
										Includes Branded Reports
									</li>
									<li class="position-item">
										<span class="count">50</span>
										Social Accounts
									</li>
								</ul>
								<h4 class="rate">$99.99</h4>
								<a href="#" class="btn btn-medium btn--dark">
									<span class="text">Order Now!</span>
									<span class="semicircle"></span>
								</a>

							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

							<div class="pricing-tables-item">

								<div class="pricing-tables-icon">
									<img src="img/pricing3.png" alt="personal">
								</div>
								<a href="#" class="pricing-title">Professional</a>
								<ul class="pricing-tables-position">
									<li class="position-item">
										<span class="count">100</span>
										Analytics Campaigns
									</li>
									<li class="position-item">
										<span class="count">7500</span>
										Keywords
									</li>
									<li class="position-item">
										<span class="count">1,250,000</span>
										Crawled Pages
									</li>
									<li class="position-item include">
										Includes Branded Reports
									</li>
									<li class="position-item">
										<span class="count">150</span>
										Social Accounts
									</li>
								</ul>
								<h4 class="rate">$169.99</h4>
								<a href="#" class="btn btn-medium btn--dark">
									<span class="text">Order Now!</span>
									<span class="semicircle"></span>
								</a>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="pricing-tables pricing-tables-colored medium-padding120 bg-white-color">
				<div class="container">

					<div class="row">

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

							<div class="pricing-tables-item bg-primary-color">

								<div class="pricing-tables-icon">
									<img src="img/pricing1.png" alt="personal">
								</div>
								<a href="#" class="pricing-title">Personal</a>
								<ul class="pricing-tables-position">
									<li class="position-item">
										<span class="count">5</span>
										Analytics Campaigns
									</li>
									<li class="position-item">
										<span class="count">300</span>
										Keywords
									</li>
									<li class="position-item">
										<span class="count">250,000</span>
										Crawled Pages
									</li>
									<li class="position-item">
										-
									</li>
									<li class="position-item">
										<span class="count">15</span>
										Social Accounts
									</li>
								</ul>
								<h4 class="rate">$49.99</h4>

								<div class="btn btn-medium btn-border">
									<span class="text">ORDER NOW!</span>
									<span class="semicircle"></span>
								</div>

							</div>

						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="pricing-tables-item bg-secondary-color">

								<div class="pricing-tables-icon">
									<img src="img/pricing2.png" alt="personal">
								</div>
								<a href="#" class="pricing-title">Webmaster</a>
								<ul class="pricing-tables-position">
									<li class="position-item">
										<span class="count">25</span>
										Analytics Campaigns
									</li>
									<li class="position-item">
										<span class="count">1,900</span>
										Keywords
									</li>
									<li class="position-item">
										<span class="count">1,250,000</span>
										Crawled Pages
									</li>
									<li class="position-item include">
										Includes Branded Reports
									</li>
									<li class="position-item">
										<span class="count">50</span>
										Social Accounts
									</li>
								</ul>
								<h4 class="rate">$99.99</h4>

								<div class="btn btn-medium btn-border">
									<span class="text">ORDER NOW!</span>
									<span class="semicircle"></span>
								</div>

							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

							<div class="pricing-tables-item bg-orange-color">

								<div class="pricing-tables-icon">
									<img src="img/pricing3.png" alt="personal">
								</div>
								<a href="#" class="pricing-title">Professional</a>
								<ul class="pricing-tables-position">
									<li class="position-item">
										<span class="count">100</span>
										Analytics Campaigns
									</li>
									<li class="position-item">
										<span class="count">7500</span>
										Keywords
									</li>
									<li class="position-item">
										<span class="count">1,250,000</span>
										Crawled Pages
									</li>
									<li class="position-item include">
										Includes Branded Reports
									</li>
									<li class="position-item">
										<span class="count">150</span>
										Social Accounts
									</li>
								</ul>
								<h4 class="rate">$169.99</h4>

								<div class="btn btn-medium btn-border">
									<span class="text">ORDER NOW!</span>
									<span class="semicircle"></span>
								</div>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection