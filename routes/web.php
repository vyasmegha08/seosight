<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace'=>'App\http\controllers'], function() {
    Route::get('/','indexController@seo')->name('index');
    Route::get('/services','ServiceController@index')->name('service.index');
    route::get('/price','PriceController@price')->name('price.index');
    route::get('/contact','contactController@contact')->name('contact.index');
    route::get('/blog','blogController@blog')->name('blog.index');
    route::get('/blog2','blog2Controller@blog2')->name('blog2.index');
});
