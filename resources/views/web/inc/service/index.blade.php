@extends('web.layouts.app')
@section('main_section')
services price
<div class="container-fluid">
		<div class="row">
			<div class="pricing-tables medium-padding120 bg-border-color">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
							<div class="heading align-center">
								<h4 class="h1 heading-title">Our Pricing Packages</h4>
								<div class="heading-line">
									<span class="short-line"></span>
									<span class="long-line"></span>
								</div>
								<p class="heading-text">Claritas est etiam processus dynamicus,
									qui sequitur mutationem consuetudium.
								</p>
							</div>
						</div>
					</div>

				<div class="row">
						<div class="pricing-tables-wrap">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

								<div class="pricing-tables-item">
									<div class="pricing-tables-icon">
										<img src="{{url('img/pricing1.png')}}" alt="personal">
									</div>
									<a href="#" class="pricing-title">Basic Bussiness Plan</a>
                                    <h4 class="rate">$49.99</h4>
									<ul class="pricing-tables-position">
										<li class="position-item">
											Free Domain (.in)
										</li>
										<li class="position-item">
											
                                             Hosting (01 Year Free)
										</li>
										<li class="position-item">
											
											4 Page (Dynamic Website)
										</li>
										<li class="position-item">
                                            Images + Video Gallery
										</li>
										<li class="position-item">
											
											Limited Images & Videos
										</li>
                                        <li class="position-item">
											
                                            Limited* (Space)
                                        </li>
                                        <li class="position-item">
											
                                            SEO Ready Website
                                        </li>
                                        <li class="position-item">
											
                                            100% Responsive Website
                                        </li>
                                        <li class="position-item">
											
                                            Hit Counter + WhatsApp Button
                                        </li>
                                        <li class="position-item">
											
                                            Business Email ID
                                        </li>
                                        <li class="position-item">
											
                                            Payment Gateway Integration
                                        </li>
                                        <li class="position-item">
											
                                            Social Media Integration
                                        </li>
                                        <li class="position-item">
											
                                            SSL Certificate
                                        </li>
                                        <li class="position-item">
											
                                            Mobile Friendly Website
                                        </li>
                                        <li class="position-item">
											
                                            Enquiry / Feedback Form
                                        </li>
                                        <li class="position-item">
											
                                            Support (Phone/Email/Chat)
                                        </li>
                                        <li class="position-item">
											
                                            One Year Technical Support
                                        </li>
                                        
                                    </ul>
									
									<a href="20_checkout.html" class="btn btn-medium btn--dark">
										<span class="text">Order Now!</span>
										<span class="semicircle"></span>
									</a>
									<img src="img/pricing-dots.png" class="dots" alt="dots">
								</div>

							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="pricing-tables-item">
									<div class="pricing-tables-icon">
										<img src="img/pricing2.png" alt="personal">
									</div>
									<a href="#" class="pricing-title">SuperBusinessPlan</a>
                                    <h4 class="rate">$99.99</h4>
									<ul class="pricing-tables-position">
                                        <li class="position-item">
											Free Domain (.in)
										</li>
										<li class="position-item">
											
                                             Hosting (01 Year Free)
										</li>
										<li class="position-item">
											
                                             6 Page (Dynamic Website)
										</li>
										<li class="position-item">
                                            Images + Video Gallery
										</li>
										<li class="position-item">
											
                                            Unlimited Images & Videos
										</li>
                                        <li class="position-item">
											
                                             Unlimited* (Space)
                                        </li>
                                        <li class="position-item">
											
                                            SEO Ready Website
                                        </li>
                                        <li class="position-item">
											
                                            100% Responsive Website
                                        </li>
                                        <li class="position-item">
											
                                            Hit Counter + WhatsApp Button
                                        </li>
                                        <li class="position-item">
											
                                            Business Email ID
                                        </li>
                                        <li class="position-item">
											
                                            Payment Gateway Integration
                                        </li>
                                        <li class="position-item">
											
                                            Social Media Integration
                                        </li>
                                        <li class="position-item">
											
                                            SSL Certificate
                                        </li>
                                        <li class="position-item">
											
                                            Mobile Friendly Website
                                        </li>
                                        <li class="position-item">
											
                                            Enquiry / Feedback Form
                                        </li>
                                        <li class="position-item">
											
                                            Support (Phone/Email/Chat)
                                        </li>
                                        <li class="position-item">
											
                                            One Year Technical Support
                                        </li>

									</ul>
									
									<a href="20_checkout.html" class="btn btn-medium btn--dark">
										<span class="text">Best Business Plan</span>
										<span class="semicircle"></span>
									</a>

									<img src="img/pricing-dots.png" class="dots" alt="dots">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="pricing-tables-item">
									<div class="pricing-tables-icon">
										<img src="img/pricing3.png" alt="personal">
									</div>
									<a href="#" class="pricing-title">Professional</a>
                                    <h4 class="rate">$169.99</h4>
									<ul class="pricing-tables-position">
                                    <li class="position-item">
											Free Domain (.in)
										</li>
										<li class="position-item">
											
                                             Hosting (01 Year Free)
										</li>
										<li class="position-item">
											
                                             L Pages* (Dynamic Website)
										</li>
										<li class="position-item">
                                            Images + Video Gallery
										</li>
										<li class="position-item">
											
                                            Unlimited Images & Videos
										</li>
                                        <li class="position-item">
											
                                            Unlimited* (Bandwidth/ Space)
                                        </li>
                                        <li class="position-item">
											
                                            SEO Ready Website
                                        </li>
                                        <li class="position-item">
											
                                            100% Responsive Website
                                        </li>
                                        <li class="position-item">
											
                                            Hit Counter + WhatsApp Button
                                        </li>
                                        <li class="position-item">
											
                                            Business Email ID
                                        </li>
                                        <li class="position-item">
											
                                            Payment Gateway Integration
                                        </li>
                                        <li class="position-item">
											
                                            Social Media Integration
                                        </li>
                                        <li class="position-item">
											
                                            SSL Certificate
                                        </li>
                                        <li class="position-item">
											
                                            Mobile Friendly Website
                                        </li>
                                        <li class="position-item">
											
                                            Enquiry / Feedback Form
                                        </li>
                                        <li class="position-item">
											
                                            Support (Phone/Email/Chat)
                                        </li>
                                        <li class="position-item">
											
                                            One Year Technical Support
                                        </li>
									</ul>
									
									<a href="20_checkout.html" class="btn btn-medium btn--dark">
										<span class="text">Order Now!</span>
										<span class="semicircle"></span>
									</a>

									<img src="img/pricing-dots.png" class="dots" alt="dots">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


@endsection